<?php get_header(); ?>

    <main>
        <section class="background">
            <div class="ajuste">
                <picture><img src="<?php echo get_stylesheet_directory_uri() ?>/media/image.png"></picture>
                <label class="frase1"><?php the_field('titulo'); ?></label>
                <label class="frase2"><?php the_field('descricao'); ?></label>
            </div>
            <div class="sobre">
                <h2 class="titSobre">Sobre</h2>
                <p class="pSobre">Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento <br> das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de <br> gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz <br> parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento <br> do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
            </div>
            <div class="valores">
                <h2 class="titValores">Valores</h2>
                <picture class="containImg">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/Protection.png" class="valor">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/care.png" class="valor">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/company.png" class="valor">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/rescue.png" class="valor">
                </picture>
                <div class="mother">
                    <div class="oldest">
                        <h3>Proteção</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                    <div class="older">
                        <h3>Carinho</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                    <div class="younger">
                        <h3>Companherismo</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div> 
                    <div class="youngest">
                        <h3>Resgate</h3>
                        <p>Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.</p>
                    </div>
                </div>
            </div>
            <div class="exemplos">
                <h2 class="titExemplos">Lobos Exemplo</h2>
                <div class="geralEsq">
                <div class="ladoEsq">
                        <div class="imgEsq">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/media/ladoEsq.png">
                        </div>
                    </div>
                    <div class="infoEsq">
                        <h4 class="titEsq">Nome Lobo</h4>
                        <h5 class="idadeEsq">Idade: xx anos</h5>
                        <p class="fraseEsq">Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
                    </div>
                </div>
                <br><br>
                <div class="geralDir">
                <div class="infoDir">
                        <h4 class="titDir">Lobo nome</h4>
                        <h5 class="idadeDir">Idade: xx anos</h5>
                        <p class="fraseDir">Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
                    </div><div class="ladoDir">
                        <div class="imgDir">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/media/ladoDir.png">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <?php get_footer(); ?>