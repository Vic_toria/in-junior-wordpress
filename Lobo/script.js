/*variaveis iniciais*/
const urlAPILobos = "http://lobinhos.herokuapp.com/wolves"

/*funcoes */ 

function pegaLobos(adotado) {

    adotado = adotado || false;
    let fetchConfig = {
        method: 'GET'
    }

    if (adotado) {

        let urlAdotados = urlAPILobos + '/adopted';
        fetch(urlAdotados, fetchConfig)
            .then((resposta) => resposta.json()
                .then((resp) => {
                    console.log(resp[5].name)
                })
                .catch((erro) => { console.log(erro)}))
            .catch((erro) => {console.log(erro)})
    }
    else {
        fetch(urlAPILobos, fetchConfig)
            .then((resposta) => resposta.json()
                .then((resp) => {
                    console.log(resp[5].name)
                })
                .catch((erro) => { console.log(erro)}))
            .catch((erro) => {console.log(erro)})
    }
}

function validaDados(nome, idade, foto, descricao) {
    if (nome.length < 4 || nome.length > 60) {
        alert('Nome deve ter entre 4 e 60 caracteres.')
        return false
    }
    else {

        idade = parseInt(idade)
        if (idade < 0 || idade > 100) {
            alert('Idade deve ser entre 0 e 100.')
            return false
        }

        else {
            if (foto.length <= 0) {
                alert('Campo de imagem vazio.')
                return false
            }

            else {
                if (descricao.length < 5 || descricao.length > 255) {
                    alert('A descrição deve ter entre 5 e 255 caracteres')
                    return false
                }
                else {
                    return true 
                }
            }
        }
    }
}

function enviaLobo(nome, idade, foto, descricao) {

    if (validaDados(nome, idade, foto, descricao)) {

        let fetchBody = {
            "wolf": {
                "name": nome,
                "age": parseInt(idade),
                "image_url": foto,
                "description": descricao
            }
        }
    
        let fetchConfig = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(fetchBody)
        }
        fetch(urlAPILobos, fetchConfig)
            .then(resposta => resposta.json()
                .then(resp => { console.log(resp) })
                .catch(erro => { console.log(erro) }))
            .catch(erro => {console.log(erro)})
    }
    else {
        return
    }
}

function mostraLobo(idlobo) {
    let urllobin = urlAPILobos + '/' + idlobo
    let fetchConfig = {
        method: 'GET'
    }
    fetch(urllobin, fetchConfig)
            .then((resposta) => resposta.json()
                .then((resp) => {console.log(resp)})
                .catch((erro) => { console.log(erro)}))
            .catch((erro) => {console.log(erro)})
}

function adotaLobo(nomedono, idadedono, emaildono, idlobo) {
    let urllobin = urlAPILobos + '/' + idlobo
        
    let fetchBody = {
        "wolf": {
            "adopter_name": nomedono,
            "adopter_age": idadedono,
            "adopter_email": emaildono
        }
    }

    let fetchConfig = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(fetchBody)
    }

    fetch(urllobin, fetchConfig)
        .then(resposta => resposta.json()
        .then(resp => { console.log(resp) })
        .catch(erro => { console.log(erro) }))
    .catch(erro => {console.log(erro)})
}

function apagaLobo(idlobo) {
    let urllobin = urlAPILobos + '/' + idlobo

    let fetchConfig = {
        method: 'DELETE'
    }
    fetch(urllobin, fetchConfig)
        .then(resposta => resposta.json()
        .then(resp => { console.log(resp) })
        .catch(erro => { console.log(erro) }))
    .catch(erro => {console.log(erro)})
}

function retonaLobin(lobin) {
    return lobin;
}

function mostraLobinhosIndex() {

    let fetchConfig = {
        method: 'GET'
    }
    fetch(urlAPILobos, fetchConfig)
            .then((resposta) => resposta.json()
                .then((resp) => {
                    console.log(resp)
                    let random1 = getRandomInt(0, resp.length)
                    let random2 = getRandomInt(0, resp.length)
                    let divMaeEsq = document.querySelector('.geralEsq')
                    let divMaeDir = document.querySelector('.geralDir')

                    divMaeEsq.innerHTML = '<div class="ladoEsq"><div class="imgEsq"><img src="' + resp[random1].image_url + '"></div></div><div class="infoEsq"><h4 class="titEsq">' + resp[random1].name + '</h4><h5 class="idadeEsq">Idade: ' + resp[random1].age + ' anos </h5><p class="fraseEsq">' + resp[random1].description + '</p></div>'
                    
                    divMaeDir.innerHTML = '<div class="infoDir"><h4 class="titDir">'+resp[random2].name+'</h4><h5 class="idadeDir">Idade: '+resp[random2].age+' anos</h5><p class="fraseDir">'+resp[random2].description+'</p></div><div class="ladoDir"><div class="imgDir"><img src="'+resp[random2].image_url+'"></div></div>'

                })
                .catch((erro) => { console.log(erro)}))
            .catch((erro) => {console.log(erro)})
    
    
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  }

pegaLobos()

