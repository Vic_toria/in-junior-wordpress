<footer>
        <iframe width="600" height="500" src="https://maps.google.com/maps?q=Av.%20Milton%20Tavares%20de%20Souza,%20s/n%20-%20Sala%20115%20B%20-%20Boa%20Viagem,%20Niter%C3%B3i%20-%20RJ,%2024210-315&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no"></iframe>
        <div class="contato"> 
            <div class="organizando">
                <picture>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/local.png">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/tel.png">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/media/email.png">
                </picture>
                <div class="infos">
                    <address>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa <br> Viagem, Niterói - RJ, 24210-315</address>
                    <p class="pFooter">(99) 99999-9999</p>
                    <a class="email" href="#">salve-lobos@lobINhos.com</a>
                </div>
                <div class="patinha">
                    <p class="pPatinha">Desenvolvido com</p>
                    <picture><img src="<?php echo get_stylesheet_directory_uri() ?>/media/paws.png"></picture>
                </div>
            </div>
            <a href="<?php echo get_stylesheet_directory_uri() ?>/quemsomos.php"><div class="quemSomosFooter">Quem Somos</div></a>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>