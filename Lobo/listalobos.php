<?php 
// Template Name: Lobinhos
?>

<?php get_header(); ?>

    <br>
    <div class="campoPesquisa">
        <picture ><img src="media/busca.png" class="lupa"></picture>
        <input type="text" class="pesquisa"> 
        <button type="button" id="addLobo">+ Lobo</button>
    </div>
    <br>
   <div>
        <input type="checkbox">
        <label>Ver lobinhos adotados</label>
    </div>
    <br><br>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="geralEsq">
            <div class="ladoEsq">
                <div class="imgEsq">
                    <?php if (get_field('ladoEsq')): ?>
                        <img src="media/ladoEsq.png">
                    <?php endif; ?>
                    <?php echo paginate_links()?>
                </div>
            </div>    
            <div class="infoEsq">
                <div class="adotando">
                    <div>
                        <h4 id="normal">Nome do Lobo</h4>
                        <h5 id="normal">Idade: xx anos</h5>
                    </div>
                    <button class="adotar" type="button"><a href="adotarlobo.html">ADOTAR</a></button>
                </div>
                <p class="fraseEsq">Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
            </div>
        </div>
    <br><br>

    <?php my_pagination(); ?>

    <?php get_footer(); ?>